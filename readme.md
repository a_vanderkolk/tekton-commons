deploy chart

```bash
rm *.tgz
helm package .
NEXUS_USER=admin
NEXUS_PASS=admin
CHART_NAME=`yq e '.name' Chart.yaml`
CHART_VERSION=`yq e '.version' Chart.yaml`
CHART="${CHART_NAME}-${CHART_VERSION}.tgz"
NEXUS_REPO=http://nexus.labs.kolkos.nl/repository/helm-hosted/
curl -v -u ${NEXUS_USER}:${NEXUS_PASS} --upload-file ${CHART} ${NEXUS_REPO}
```