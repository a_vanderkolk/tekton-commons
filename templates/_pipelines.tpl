{{/*
Names of the pipelines
*/}}
{{- define "pipeline.names" -}}
build:
  java: build-java-app
  springBoot: build-spring-app
  angular: build-angular-app
  helm: build-helm-chart
{{- end }}

{{/*
Parameter names for global project variables
*/}}
{{- define "pipeline.global.project.parameters" -}}
giteaUrl: 
  name: gitea-url
  description: The URL of the Gitea instance
sonarUrl: 
  name: sonarqube-url
  description: The URL of SonarQube
fullName: 
  name: full-project-name
  description: The organization / repositoryname from Gitea
safeName: 
  # TODO: Deze is waarschijnlijk niet meer nodig als ik de informatie uit de pom kan halen
  name: safe-project-name
  description: The project name used for Sonar (stripped of any unsafe characters)
pullRequestId: 
  name: pull-request-id
  description: The ID of the pull request that triggers the build
commitId: 
  name: commit-id
  description: The ID of the latest commit
{{- end }}

{{/*
Parameter names for global build variables
*/}}
{{- define "pipeline.global.build.parameters" -}}
repoUrl: 
  name: source-repository-url
  description: The url of the repository containing the source code to build
repoRevision: 
  name: source-repository-revision
  description: The revision (branch, tag, sha) to clone
imageName: 
  name: image-name
  description: The name of the Image with the Dockerfile
{{- end }}

