{{- define "pom-reader" -}}
groupId:
  description: The GroupID defined in the pom.xml file  
  resultName: group-id
  xpath: '.project.groupId'
artifactId:
  description: The ArtifactID defined in the pom.xml file  
  resultName: artifact-id
  xpath: '.project.artifactId'
version:
  description: The Application Version defined in the pom.xml file  
  resultName: version
  xpath: '.project.version'
{{- end }}