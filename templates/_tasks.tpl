{{- define "tasks.names" -}}
buildah: buildah
cat: cat
checkQualityGate: sonar-check-quality-gate
deployArgoCDApplication: deploy-argocd-application
gitCli: git-cli
gitClone: git-clone
gitCreateBranch: git-create-branch
giteaCreateInfraPR: gitea-create-infra-pr
giteaCreatePR: gitea-create-pr
giteaCreateWebhook: gitea-create-webhook
helmPackage: helm-package
helmUpgradeFromRepo: helm-upgrade-from-repo
helmUpgradeFromSource: helm-upgrade-from-source
helmGetChartInfo: helm-read-chart-details
jq: jq
kubernetesActions: kubernetes-actions
listFolder: debug-list-folder
maven: maven
mavenGetApplicationVersion: maven-get-application-version
mavenPomReader: maven-pom-reader
mavenSnapshotRemover: maven-snapshot-remover
mavenVersionUpdater: maven-version-updater
nexusPushHelmChart: nexus-push-helm-chart
nexusCheckArtifactExists: nexus-artifact-exists-checker
npm: npm
randomId: debug-random-id
readTektonfileBuild: get-build-configuration
reportBuildStatus: gitea-report-build-status
skopeoCopy: skopeo-copy
sonarqubeScanner: sonarqube-scanner
startIaCPipeline: start-iac-pipeline
tkn: tkn
updateChartYaml: update-chart-yaml
updateHelmChart: update-helm-chart
updateValuesYaml: update-values-yaml
uploadToNexus: upload-to-nexus
{{- end }}

{{- define "custom-task-images" -}}
pythonAgent:
  repository: 192.168.178.21:32000
  image: tekton/python-agent:latest
  tag: 1.0.0
yamlAgent:
  repository: 192.168.178.21:32000
  image: tekton/yaml-tekton-agent
  tag: 0.0.1
{{- end }}