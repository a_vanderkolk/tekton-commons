{{- define "tektonfile.build" -}}
sourceContextDir:
  description: The path containing the Source Code To build
  resultName: context-dir
  yamlPath: '.source.contextDir'
  defaultValue: "."
sourceDockerfile:
  description: The location of the Dockerfile to use when building the Image
  resultName: dockerfile-location
  yamlPath: '.source.dockerfile'
  defaultValue: "./Dockerfile"
iacUpdateRepository:
  description: Whether to update a IaC repository
  resultName: update-iac-repository
  yamlPath: '.iac.updateRepository'
  defaultValue: "false"
iacDeleteExistingBranch:
  description: Whether to delete the existing feature branch in the IaC repository or not
  resultName: delete-existing-iac-branch
  yamlPath: '.iac.deleteBranch'
  defaultValue: "false"
iacRepositoryUrl:
  description: The URL of the repository containing the IaC definition for this project. 
  resultName: iac-repository-url
  yamlPath: '.iac.repositoryUrl'
  defaultValue: ""
iacRepositoryBranch:
  description: The branch of the Repository to clone.
  resultName: iac-repository-branch
  yamlPath: '.iac.repositoryBranch'
  defaultValue: "development"
iacContextDir:
  description: The directory containing the Helm Chart to update
  resultName: iac-context-dir
  yamlPath: '.iac.contextDir'
  defaultValue: "."
iacImageKey:
  description: The key in the 'values.yaml' containing the Image name/location
  resultName: iac-image-key
  yamlPath: '.iac.valuesYaml.image'
  defaultValue: ".image.repository"
iacDigestKey:
  description: The key in the 'values.yaml' containing the digest of the image
  resultName: iac-digest-key
  yamlPath: '.iac.valuesYaml.tag'
  defaultValue: ".image.tag"
{{- end }}